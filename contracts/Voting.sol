// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.8.22;

import "@openzeppelin/contracts/access/Ownable.sol";

/**
 * @title Voting
 * @dev Voting system
 */
contract Voting is Ownable(msg.sender) {

    // -- Structures -- 

    struct Voter {
        bool isRegistered;
        bool hasVoted;
        uint votedProposalId;
        uint tokenBalance;
    }

    struct Proposal {
        string description;
        uint voteCount;
    }
    
    enum WorkflowStatus {
        RegisteringVoters,
        ProposalsRegistrationStarted,
        ProposalsRegistrationEnded,
        VotingSessionStarted,
        VotingSessionEnded,
        VotesTallied
    }

    uint public winningProposalId;
    WorkflowStatus public currentWorkflowStatus = WorkflowStatus.VotesTallied;

    mapping(address => Voter) public whiteList;
    mapping(uint => Proposal) public proposals;
    mapping(address => uint) public tokenBalances;

    uint public numProposals;

    // -- Modifiers -- 

    modifier onlyInStatus(WorkflowStatus _requiredStatus) {
        require(currentWorkflowStatus == _requiredStatus, "Invalid workflow status");
        _;
    }

    // -- Evenements -- 

    event VoterRegistered(address voterAddress);
    event VoterUnregistered(address voterAddress);
    event WorkflowStatusChange(WorkflowStatus previousStatus, WorkflowStatus newStatus);
    event ProposalRegistered(uint proposalId);
    event ProposalUnregistered(uint proposalId);
    event Voted(address voter, uint proposalId);

    // -- Fonctions --

    function setWorkflowStatus(WorkflowStatus _newStatus) private {
        emit WorkflowStatusChange(currentWorkflowStatus, _newStatus);
        currentWorkflowStatus = _newStatus;
    }

    function addAddressToWhiteList(address _newAddress) public onlyOwner {
        whiteList[_newAddress] = Voter(true, false, 0, 0);
        emit VoterRegistered(_newAddress);
    }

    function removeAddressFromWhiteList(address _addressToRemove) public onlyOwner {
        delete whiteList[_addressToRemove]; 
        emit VoterUnregistered(_addressToRemove);
    }

    function beginRegisteringProposals() public onlyOwner {
        setWorkflowStatus(WorkflowStatus.ProposalsRegistrationStarted);
    }

    function endProposalsRegistration() public onlyOwner onlyInStatus(WorkflowStatus.ProposalsRegistrationStarted) {
        setWorkflowStatus(WorkflowStatus.ProposalsRegistrationEnded);
    }

    function beginVotingSession() public onlyOwner {
        setWorkflowStatus(WorkflowStatus.VotingSessionStarted);
    }

    function endVotingSession() public onlyOwner onlyInStatus(WorkflowStatus.VotingSessionStarted) {
        setWorkflowStatus(WorkflowStatus.VotingSessionEnded);
    }

    function addProposal(string memory _description) public onlyInStatus(WorkflowStatus.ProposalsRegistrationStarted) {
        proposals[numProposals] = Proposal({
            description: _description,
            voteCount: 0
        });
        numProposals++;
        emit ProposalRegistered(numProposals);
    }

    function removeProposal(uint _proposalId) public onlyOwner{
        delete proposals[_proposalId];
        numProposals--;
        emit ProposalUnregistered(_proposalId);
    }

    function vote(uint _proposalId) public {
        require(whiteList[msg.sender].isRegistered, "You are not registered as a voter");
        require(!whiteList[msg.sender].hasVoted, "You have already voted");
        require(currentWorkflowStatus == WorkflowStatus.VotingSessionStarted, "Voting session has not started yet");
        require(_proposalId < numProposals, "Invalid proposal ID");
        require(tokenBalances[msg.sender] > 0, "You have no tokens to vote");

        proposals[_proposalId].voteCount += tokenBalances[msg.sender];
        whiteList[msg.sender].hasVoted = true;
        whiteList[msg.sender].votedProposalId = _proposalId;

        emit Voted(msg.sender, _proposalId);
    }

    function countVotes() public onlyOwner onlyInStatus(WorkflowStatus.VotingSessionEnded) {        
        uint winningCount = 0;
        uint winningProposal = 0;

        for (uint i = 1; i < numProposals; i++) {
            if (proposals[i].voteCount > winningCount) {
                winningCount = proposals[i].voteCount;
                winningProposal = i;
            }
        }

        winningProposalId = winningProposal;
        setWorkflowStatus(WorkflowStatus.VotesTallied);
    }    

    function getWinningProposal() public view returns (uint) {
        require(currentWorkflowStatus == WorkflowStatus.VotesTallied, "Votes have not been tallied yet");
        return winningProposalId;
    }

    function distributeTokens(address[] memory recipients, uint[] memory amounts) public onlyOwner {
        require(currentWorkflowStatus == WorkflowStatus.RegisteringVoters, "Tokens can only be distributed during voter registration");
        require(recipients.length == amounts.length, "Arrays must have the same length");

        for (uint i = 0; i < recipients.length; i++) {
            address recipient = recipients[i];
            uint amount = amounts[i];
            require(whiteList[recipient].isRegistered, "Recipient is not registered as a voter");
            tokenBalances[recipient] = amount;
        }
    }

    function getTokenBalance(address voter) public view returns (uint) {
        return tokenBalances[voter];
    }
}